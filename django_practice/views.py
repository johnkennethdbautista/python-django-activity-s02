from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Local imports
from .models import GroceryItem

# Create your views here.
def index(request):
	groceryitem_list = GroceryItem.objects.all()
	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# template = loader.get_template('todolist/index.html')
	context = {'groceryitem_list': groceryitem_list}
	return render(request, 'django_practice/index.html', context)

def groceryitem(request, groceryitem_id):
	response = 'You are viewing the details of %s'
	return HttpResponse(response % groceryitem_id)