from django.urls import path
from . import views

# You can declare routes by using the path function and having 3 arguments within that function.
# 1st argument - endpoint, 2nd argument - function to be triggered, 3rd argument - label for the route
urlpatterns = [
	path('', views.index, name = 'index'),
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem')
]